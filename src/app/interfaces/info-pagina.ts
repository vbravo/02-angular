
export interface InfoPagina {
  titulo?: string;
  email?: string;
  facebook?: string;
  instagram?: string;
  twitter?: string;
}

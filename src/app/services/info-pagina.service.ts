import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoPagina } from '../interfaces/info-pagina';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {

  info: InfoPagina = {};
  cargada = false;

  equipo: any[] = [];

  constructor(  private http: HttpClient) {
    this.CargarInfo();
    this.CargarEquipo();
   }


   private CargarInfo(){
    this.http.get('assets/data/data-pagina.json')
    .subscribe( (resp:  InfoPagina) => {
      this.cargada = true;
      this.info = resp;
    });
   }


   private CargarEquipo(){
    this.http.get('https://artesanias-patricia.firebaseio.com/equipo.json')
    .subscribe( (resp: any[]) => {

      this.equipo = resp;
      console.log(resp);

    });
   }

}
